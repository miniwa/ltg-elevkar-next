var $buoop = {required:{e:17,f:+99999,o:-3,s:9,c:76},insecure:true,unsupported:true,api:2019.12,text_for_f:{
  'msg':'Firefox har ej stöd för nya web standarder.',
  'msgmore': 'Som ett resultat ser denna hemsidan riktigt dålig ut på Firefox. Vänligen använd Chrome, Safari eller Edge istället.', 
  'bupdate': 'Ladda ner Google Chrome', 
},
reminderClosed: 0,
reminder: 0,
url: "https://www.google.se/chrome/",
}; 
function $buo_f(){ 
 var e = document.createElement("script"); 
 e.src = "https://browser-update.org/update.min.js"; 
 document.body.appendChild(e);
};
try {document.addEventListener("DOMContentLoaded", $buo_f,false)}
catch(e){window.attachEvent("onload", $buo_f)}