import Layout from '../components/Layout';
import Link from 'next/link';

export default function Index() {
  return (
    <Layout>
      <main>
      <style jsx>{`
        main {
          width: 80%;
          padding-left: 10%;
          padding-right: 10%;
        }
        h1 {
          margin-top: 0;
          padding-top: 1rem;
        }
        a {
          color: var(--fg);
          text-decoration: none;
          border-bottom: 2px solid var(--ltg);
        }
      `}</style>
        <h1>Kontakta oss</h1>
        <h2>Epost</h2>
        <p>Få tag i oss på <a href="mailto:kontakt@ltgelevkar.se">kontakt@ltgelevkar.se</a>! Tack på förhand :)<br />Vill du ha tag i en individuell medlemm, kolla <Link href="/styrelsen"><a>styrelse</a></Link> sidan.</p>
        <h2>Sociala Medier</h2>
        <ul>
          <li>Instagram<br />Vi har en instagram på <a href="https://www.instagram.com/lindholmenselevkar/">lindholmenselevkar</a></li><br />
          <li>Facebook<br />Vi har ett facebook konto på <a href="https://www.facebook.com/LindholmensElevkar">LindholmensElevkar</a></li><br />
          <li>Discord<br />Vi har en discord chatt på <a href="https://discord.gg/CB3b8Xn">CB3b8Xn</a></li>
        </ul>
        Kom och säg hej, vi svarar på alla frågor!<br /><br />
      </main>
    </Layout>
  );
}