import Layout from '../components/Layout';
import Styrelsencontainer from '../components/Styrelsecontainer';

export default function Index() {
  return (
    <Layout>
      <main>
        <style jsx>{`
        main {
          width: 80%;
          padding-left: 10%;
          padding-right: 10%;
        }
        h1 {
          margin-top: 0;
          padding-top: 1rem;
        }
        div {
          display: flex;
          justify-content: space-around;
          flex-flow: wrap;
        }
        a {
          color: var(--fg);
          text-decoration: none;
          border-bottom: 2px solid var(--ltg);
        }
      `}</style>
        <h1>Styrelsen</h1><br />
        <h2>NOTERA: GAMLA STYRELSEMEDLEMMAR!!! Kolla facebook/instagram för uppdaterad info. --remi</h2><br />
        <div>
          <Styrelsencontainer name="Linus Björklund" role="Ordförare" img="/linus.jpg" id="linus" border={{border: '0.5rem solid red'}} email="mailto:linus.bjorklund2001@gmail.com" phone="tel:0763400135">
            Info om Linus här... kan ni tro att vi har gått ur elevkåren, men det ändå inte står någon info om oss? :>
          </Styrelsencontainer>
          <Styrelsencontainer name="Remilia Wiberg" role="Kassör, IT-Ansvarig" img="/remi.jpg" id="Remilia" border={{border: '0.5rem solid green'}} email="mailto:remilia@remilia.se" phone="tel:0735161756">
            Hej, jag är Remilia! Jag är den som gjorde denna hemsidan :) Om den nya elevkårsstyrelsen vill ha domänen - få tag i mig, jag kan hjälpa er överföra den :)
          </Styrelsencontainer>
          <Styrelsencontainer name="Erik Johannesson" role="Sekreterare" img="/erik.jpg" id="erik" border={{border: '0.5rem solid blue'}} email="mailto:eric.jnet@telia.com" phone="tel:0705888595">
            Info om erik här...
          </Styrelsencontainer>
          <Styrelsencontainer name="David Merits" role="Vice Ordförare" img="/davis.jpg" id="david" border={{border: '0.5rem solid red'}} email="mailto:david.merits@hotmail.se" phone="tel:0761607084">
            Info om david här...
          </Styrelsencontainer>
        </div>
        <h2>Är du våran nästa styrelsemedlem? Ta kontakt med någon av oss om du är intresserad!<br />Just nu söker vi speciellt efter 1:or.</h2>
        <p>Är du företag, eller vill bara ha kontakt med elevkåren generellt? Eposta oss då på <a href="mailto:kontakt@ltgelevkar.se">kontakt@ltgelevkar.se</a>! Tack på förhand :)</p>
        <br />
      </main>
    </Layout>
  );
}
