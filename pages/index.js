import Layout from '../components/Layout';
import Speen from '../components/Speen';

export default function Index() {
  return (
    <Layout>
      <Speen />
    </Layout>
  );
}