import Layout from '../components/Layout';

export default function Index() {
  return (
    <Layout>
      <main>
        <style jsx>{`
          iframe {
            display: block;
            width: 100%; min-height: 50rem;
          }
        `}</style>
        <iframe src="https://ebas.gymnasiet.sverigeselevkarer.se/signups/index/1043" frameborder="0" />
      </main>
    </Layout>
  );
}