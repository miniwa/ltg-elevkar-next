import Layout from '../components/Layout';
import Eventcontainer from '../components/Eventcontainer';
import Hr from '../components/Hr';

export default function Index() {
  return (
    <Layout>
      <main>
        <style jsx>{`
        main {
          width: 80%;
          padding-left: 10%;
          padding-right: 10%;
        }
        h1 {
          margin-top: 0;
          padding-top: 1rem;
        }
      `}</style>
        <h1>Event</h1><br />
        <Eventcontainer id="lucia" title="Luciafika" img="/event/lussebulle.jpg" alt="bulle">
          <p>Snart kommer tomten! Men innan det har vi lussefika.<br />
          Kom på fredag den 13 december, Kl 10:20 - 13:30, på torget i äran våning 4.<br />
          Allting kostar 5 kr, vi kommer ha kaffe, saft, bullar och mer!<br />
          Betalning skers genom kontant eller kort. (Kortgräns på 10 kr.)</p>
        </Eventcontainer><Hr />
        <Eventcontainer id="dnd" title="Dungeons and Dragons" img="/event/dnd.jpg" alt="dungeon">
          <p>Dungeons and dragons klubb!!</p>
        </Eventcontainer><Hr />
        <Eventcontainer id="fest" title="Julfest" img="/event/julkula.png" alt="julkula">
          <p>Biljetter kostar 240kr för medlemmar (och 260 för icke-medlemmar), säljes:</p>
          <ul>
            <li>Måndag den 9 december, Kl 14:40 - 15:20, Äran plan 4, torget</li>
            <li>Tisdag den 10 december, Kl 14:05 - 14:45, Utanför Santos, Sal 11</li>
            <li>Onsdag den 11 december, Kl 14:20 - 15:00, Bohus</li>
          </ul>
          <p>Betalning skers genom kontant eller kort.<br />
            Missa inte!</p>
        </Eventcontainer>
      </main>
    </Layout>
  );
}
