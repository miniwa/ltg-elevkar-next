import Header from './Header';
import Head from 'next/head';
import Footer from './Footer';

const Layout = props => (
  <div>
    <Head>
      <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
      <title>Lindholmens Tekniska Elevkår</title>
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <link href="https://fonts.googleapis.com/css?family=Muli&display=swap" rel="stylesheet" />
      <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
      <meta name="theme-color" content="#850066" />
      <meta name="description" content="Lindholmens tekniska elevkår!" />
    </Head>
    <style>{`
      :root {
        --lbg: #fff;
        --bg: #fff;
        --bg2: #eee;
        --lfg: #000;
        --fg: #000;
        --fg2: #444;
        --lfg2: #444;
        --ltg: #850066;
      }
      
      body {
        margin: 0;
        padding: 0;
        background-color: var(--bg);
        font-family: 'Muli', sans-serif;
        color: var(--fg);
      }

      @media (prefers-color-scheme: dark) {
        :root {
          --bg: #222;
          --bg2: #333;
          --fg: #eee;
          --fg2: #aaa;
        }
      }

      main {
        background-color: var(--bg2);
      }
    `}</style>
    <Header />
    {props.children}
    <Footer />
    <script src="/stoppls.js" />
  </div>
);

export default Layout;