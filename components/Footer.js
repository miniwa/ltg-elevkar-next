import Link from 'next/link';

const Footer = () => (
  <footer>
    <style jsx>{`
footer {
  width: 80%;
  padding-left: 10%;
  padding-right: 10%;
  border-top: 4px solid var(--lfg2);
  min-height: 20vh;
  display: flex;
  flex-wrap: wrap;
  justify-content: flex-start;
}

footer a {
  text-decoration: none;
  color: var(--fg);
}

footer p {
  line-height: 1.5;
}

#fcontact, #fevent, #fetc {
  min-width: 30%;
  margin-right: 2rem;
}

@media (max-width: 768px) {
  footer p {
    font-size: 1.2rem;
    line-height: 2;
  }
}
`}</style>
    <div id="fcontact">
      <h2>Kontakt</h2>
      <p>
        <Link href="/styrelsen#linus"><a>🤴 Linus Björklund</a></Link><br />
        <Link href="/styrelsen#Remilia"><a>🏦 Remilia Wiberg</a></Link><br />
        <Link href="/styrelsen#erik"><a>💻 Erik Johannesson</a></Link><br />
        <Link href="/styrelsen#david"><a>🤴 David Merits</a></Link><br />
        <a href="https://discord.gg/CB3b8Xn">💬 Chat</a>
      </p>
    </div>
    <div id="fevent">
      <h2>Event</h2>
      <p>
        <Link href="/event#lucia"><a>🧁 Luciafika</a></Link><br />
        <Link href="/event#dnd"><a>🐉 Dungeons And Dragons</a></Link><br />
        <Link href="/event#fest"><a>🎅 Julfest</a></Link>
      </p>
    </div>
    <div id="fetc">
      <h2>Mera</h2>
      <p>
        <a href="https://lindholmen.club/ekid">💳 Elevkårs id (kontakta remilia om ni vill ha källkod till detta, dött som fan lmao)</a><br />
        <a href="https://mail.egensajt.se/">📧 Webbmail</a><br />
        <a href="https://link.ltgelevkar.se/">🔗 Länkar</a><br />
      </p>
    </div>
  </footer>
);

export default Footer;
