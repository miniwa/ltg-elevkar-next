import Link from 'next/link';

const Speen = () => (
  <main>
    <style jsx>{`
#speen {
  background: url("/ltgnew.jpg") no-repeat center center fixed;
  background-size: cover;
  height: calc(100vh - 10rem);
  display: flex;
  justify-content: center;
  align-items: center;
}

#speendiv {
  -webkit-backdrop-filter: blur(10px);
  backdrop-filter: blur(10px);
  padding: 2rem;
  color: var(--lbg);
  text-shadow: 1px 1px 3px black;
  border-radius: 1rem;
  background-color: rgba(255,255,255,0.2);
  box-shadow: 0 1px 3px rgba(0,0,0,0.24), 0 1px 2px rgba(0,0,0,0.48);
  text-align: center;
}

button {
  padding: 1rem;
  font-size: 1.25rem;
  background-color: rgba(133, 0, 102, 0.4);
  color: white;
  border: 2px solid var(--ltg);
  border-radius: 1rem;
}

@media (prefers-color-scheme: dark) {
  #speen {
    background: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.3)), url("/ltgnew.jpg") no-repeat center center fixed;
    background-size: cover;
  }
}
    `}</style>
    <div id="speen">
      <div id="speendiv">
        <h1>Lindholmens Tekniska Elevkår</h1>
        <p>Hej nya elevkåren - få tag i Remilia (remilia@remilia.se) så fixar jag över hemsidan till er :)</p>
      </div>
    </div>
  </main>
);

export default Speen;
