const Styrelsecontainer = props => (
  <styrelsecontainer id={props.id}>
    <style jsx>{`
        img {
          border-radius: 9999999px;
          width: 10rem;
          height: 10rem;
          box-shadow: 0 1px 3px rgba(0,0,0,0.24), 0 1px 2px rgba(0,0,0,0.48);
        }
        styrelsecontainer {
          width: 12rem;
          display: flex;
          flex-flow: column wrap;
          justify-content: flex-start;
          text-align: center;
          padding: 1rem;
        }
        a {
          color: var(--fg);
          text-decoration: none;
          border-bottom: 2px solid var(--ltg);
        }
      `}</style>
    
      <div>
        <img src={props.img} alt={props.name} style={props.border} />
      </div>
      <h2>{props.name}</h2>
      <div>
        <p><b>{props.role}</b></p>
        <p>{props.children}</p>
        Kontakt: <a href={props.email}>Epost</a> - <a href={props.phone}>Telefon</a>
      </div>
  </styrelsecontainer>
);

export default Styrelsecontainer;