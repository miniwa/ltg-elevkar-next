const Hr = () => (
  <horline>
    <style jsx>{`
        horline {
          position: absolute;
          width: 80%;
          height: 0.2rem;
          color: transparent;
          background: linear-gradient(90deg, rgba(0,0,0,0) 0%, rgba(0,0,0,1) 20%, rgba(0,0,0,1) 80%, rgba(0,0,0,0) 100%);
        }
        @media (prefers-color-scheme: dark) {
          horline {
            background: linear-gradient(90deg, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 20%, rgba(255,255,255,1) 80%, rgba(255,255,255,0) 100%);
          }
        }
      `}</style>
  </horline>
);

export default Hr;