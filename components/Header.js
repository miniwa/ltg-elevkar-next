import Link from 'next/link';

const Header = () => (
  <header>
    <style jsx>{`
header {
  width: 80%;
  padding-left: 10%;
  padding-right: 10%;
  min-height: 10rem;
  max-height: 10rem;
  transition: max-height 1s;
  overflow; hidden;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border-bottom: 4px solid var(--lfg2);
}
#hlogo img {
  height: 8rem;
  padding: 1rem;
}
#hlinks a {
  text-transform: uppercase;
  color: var(--fg);
  text-decoration: none;
  display:inline-block;
  padding-left: 1rem;
  font-size: 1.2rem;
}
#hlinks a:after {
  transform: scaleX(0);  
  transition: transform 250ms ease-in-out;
  display:block;
  border-bottom: 2px solid var(--ltg);
  content: '';
}
#hlinks a:hover:after {
  transform: scaleX(1);  
  transition: transform 250ms ease-in-out;
}
@media (prefers-color-scheme: dark) {
  #hambaga img {
    filter: invert(0.935);
  }
}
#hspacer, #hambaga {
  display: none;
  min-width: 32px;
  min-height: 32px;
  max-width: 32px;
  max-height: 32px;
}
@media (max-width: 768px) {
  header {
    flex-wrap: wrap;
    overflow: hidden;
    padding-left: 5%;
    padding-right: 5%;
    width: 90%;
  }
  #hspacer {
    display: block;
  }
  #hlinks {
    display: block;
    min-height: 5rem;
    width: 100%;
    text-align: center;
    line-height: 3rem;
  }
  #hlinks a {
    padding-right: 0.5rem;
    padding-left: 0.5rem;
  }
  #hambaga {
    display: block;
  }
}
    `}</style>
    <div id="hspacer">
      <br />
    </div>
    <div id="hlogo">
      <Link href="/"><a><picture><source srcSet="/favicon.webp" type="image/webp"></source><source srcSet="/favicon.png" type="image/png"></source><img src="/favicon.png" alt="logga"></img></picture></a></Link>
    </div>
    <div id="hambaga" onClick={() => expand()}>
      <img src="/hambaga.svg" alt="hamabaga menu" />
    </div>
    <div id="hlinks">
      <Link href="/"><a>Hem</a></Link>
      <Link href="/event"><a>Event</a></Link>
      <Link href="/styrelsen"><a>Styrelsen</a></Link>
      <Link href="/kontakt"><a>Kontakt</a></Link>
      <Link href="/reg"><a>Bli medlem</a></Link>
    </div>
    <script src="/header.js" />
  </header>
);

export default Header;