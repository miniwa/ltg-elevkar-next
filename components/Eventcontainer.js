const Eventcontainer = props => (
  <div id={props.id}>
    <h2>{props.title}</h2>
    <style jsx>{`
        img {
          border-radius: 2rem;
          width: 15rem;
          box-shadow: 0 1px 3px rgba(0,0,0,0.24), 0 1px 2px rgba(0,0,0,0.48);
        }
        eventcontainer {
          display: flex;
          justify-content: space-between;
          flex-wrap: wrap;
        }
      `}</style>
    <eventcontainer>
      <div>
        {props.children}
      </div>
      <div>
        <img src={props.img} alt={props.alt} />
      </div>
    </eventcontainer>
    <br />
  </div>
);

export default Eventcontainer;